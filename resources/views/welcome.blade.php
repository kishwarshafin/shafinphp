<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Kishwar Shafin</title>

	<link href="/css/app.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/home.css" />
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
<header>
	<nav>
		<ul>
			<li><a href="#">PORTFOLIO</a></li>
			<li><a href="#">ABOUT</a></li>
			<li><a href="#">CONTACT</a></li>
			<li><a href="#">RESUME</a></li>
		</ul>
	</nav>
</header>
<div class="outer">
	<div class="overlay"></div>
	<div class="inner">
		<p data-shadow="Kishwar Shafin">Kishwar Shafin</p>
		<p>
			Software Engineer<span>|</span>
		</p><br>
		<a href="http://www.kishwarshafin.com"><button>View Portfolio</button></a>
		<a href="http://www.kishwarshafin.com"><button> Contact Me</button></a>
	</div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="/js/home.js"></script>
</body>
</html>