<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

    <link href="/css/app.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/test.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="outer">
    <div class="overlay"></div>
    <div class="inner">
        <h1 data-shadow="Mark Murray">Mark Murray</h1>
        <h2>
            Developer � Designer � Photographer � Musician � Multimedia Student
        </h2>
        <br>
        <a href="http://www.markmurray.co">
            <button>

                View Portfolio

            </button>
        </a>
        <a href="http://www.markmurray.co">
            <button>

                Contact Me

            </button>
        </a>
        <br>
        <br>
        <h2>Change Background Image:</h2>
        <br>
        <ul id="images">
            <li class="active"><img src="http://markmurray.co/template/images/motorway-optimized.jpg" /></li>
            <li><img src="http://markmurray.co/template/images/spraycans-optimized.jpg" /></li>
            <li><img src="http://markmurray.co/template/images/pool-optimized.jpg" /></li>
            <li><img src="http://markmurray.co/template/images/mrt-optimized.jpg" /></li>
        </ul>
    </div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="/js/onno.js"></script>
</body>
</html>