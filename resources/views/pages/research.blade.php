<!DOCTYPE html>
<html lang="en" class="demo1 no-js">

<head>

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" id="favicon" href="favicon.png">

    <title>Kishwar Shafin</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" id="favicon" href="images/favicon.png">
    <link rel="stylesheet" type="text/css" href="css/research.css" />
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>


</head>

<body class="paper">


</body>


<div class="container" style="top:20px; position: relative;">


    <div class="row text-center">

        <h1 class="col-sm-12"><img src="{{asset('images/logo_CBAC.gif')}}" width="100">Impact of Heuristics in Clustering Large Biological Networks</h1>

        <p>
            <a href=""> Md Kishwar Shafin</a>,
            <a href=""> Kazi Lutful Kabir</a>,
            <a href=""> Iffatur Ridwan</a>,
            <a href=""> Tasmiah Tamzid Anannya</a>,
            <a href=""> Rashid Saadman Karim</a>,
            <a href=""> Mohammad Mozammel Hoque</a>
            and <a href="http://teacher.buet.ac.bd/msrahman/">M. Sohel Rahman</a>
        </p>

    </div>

    <div class="row text-center">


        <div class="col-sm-6">
            <h3>Impact of Heuristics in Clustering Large Biological Networks</h3>


            <p class="text-left">Traditional clustering algorithms often exhibit poor performance for large networks.
                On the contrary, greedy algorithms are found to be relatively efficient while
                uncovering functional modules from large biological networks. The quality of the
                clusters produced by these greedy techniques largely depends on the underlying heuristics
                employed. Different heuristics based on different attributes and properties perform
                differently in terms of the quality of the clusters produced. This motivates us to design
                new heuristics for clustering large networks. In this paper, we have proposed two new
                heuristics and analyzed the performance thereof after incorporating those with three
                different combinations in a recently celebrated greedy clustering algorithm named SPICi.
                We have extensively analyzed the effectiveness of these new variants.
                The results are found to be promising.</p>
        </div>


        <div class="col-sm-6">
            <h3>Source Code</h3>
            <p><a href="https://drive.google.com/open?id=0B5olJPjfoo7JflV2VUo4cXdTT1ZPTjFxTGxJakItb1VBYU1NV2UwaC1LQVljRE94c2JSUms" target="_blank"> Download From Google Drive</a> </p>
            <p><a href="https://bitbucket.org/kishwarshafin/improvedclusteringalgorithms" target="_blank">Git</a></p>

            <h3>Test Network</h3>
            <p>Download from <a href="https://bitbucket.org/kishwarshafin/ppinetworks/downloads" target="_blank">Git</a></p>
            <p>Test networks are taken from <a href="http://compbio.cs.princeton.edu/spici/" target="_blank">SPICi</a></p>

            <h3>Paper</h3>
            The accepted manuscript can be found at <a href="http://www.sciencedirect.com/science/article/pii/S1476927115300153" target="_blank">ScienceDirect</a>.
            <br/><a href="http://tinyurl.com/qemxu58" target="_blank"> PDF from ResearchGate</a>
        </div>
    </div>

    <div class="row text-center">

        <h3>How to use the Code and Test Data</h3>
        <p style="text-align: left; font-size: large;">
            1. Download Source Codes and Test Networks.
            <p style="text-align: left"><img src="{{asset('images/step1.png')}}" width="1000" style="border:1px solid #021a40;"></p>
        </p>
        <p style="text-align: left; font-size: large;">
            2. Download <a href="http://www.codeblocks.org/downloads/binaries" target="_blank">Code::Blocks</a>.
            Note: IF UNSURE, USE "codeblocks-13.12mingw-setup.exe"!
            <p style="text-align: left"><img src="{{asset('images/step2.png')}}" width="600" style="border:1px solid #021a40;"></p>
        </p>

        <p style="text-align: left; font-size: large;">
            3. Open the cpp file of the algorithm you want to run with Code::Blocks
            <p style="text-align: left"><img src="{{asset('images/step3.png')}}" width="1000" style="border:1px solid #021a40;"></p>
        </p>
        <p style="text-align: left; font-size: large;">
            4. Rename the input file to in.txt and copy it to the directory where the cpp file is. The cpp file and input file must be in the same directory/folder.
            <p style="text-align: left"><img src="{{asset('images/step4.png')}}" width="600" style="border:1px solid #021a40;"></p>
        </p>
        <p style="text-align: left; font-size: large;">
            5. Run the code. You can press F9 too.
            <p style="text-align: left"><img src="{{asset('images/step5.png')}}" width="600" style="border:1px solid #021a40;"></p>
        </p>
        <p style="text-align: left; font-size: large;">
            6. The output will be saved in the same folder named "out.txt".
            <p style="text-align: left"><img src="{{asset('images/step6.png')}}" width="600" style="border:1px solid #021a40;"></p>
        </p>

        <p style="text-align: left; font-size: large;">
           7. To change threshold values of the algorithm please find these lines in cpp files and change the values. You can use ctrl+f to enable find option.

        <p style="text-align: left; font-size: medium;">In SPICi+1 please change the values from this line: <code>void perform_clustering(double Ts=0.5,double Ds=0.5,int min_cluster_size=0)</code></p>
        <p style="text-align: left; font-size: medium;">In SPICi+2 please change the values from these lines of main function: <code>a.set_Ts(0.5); a.set_Td(0.5);</code></p>
        <p style="text-align: left; font-size: medium;">In SPICi+12 please change the values from this line: <code>void perform_clustering(double Ts=0.5,double Ds=0.5,int min_cluster_size=0)</code></p>
        </p>
    </div>
</div>

</html>